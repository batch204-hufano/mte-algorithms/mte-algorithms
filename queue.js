let collection = [];

// Write the queue functions below.

function print() {

    return collection;
}

function enqueue(element) {

    collection[collection.length] = element
    return collection;
}

function dequeue() {

    let deletedCollection = []
    
    for (let i = 1; i < collection.length; i++) {
        deletedCollection[deletedCollection.length] = collection[i]
    }
    collection  = deletedCollection;
    return collection;
}

function front() {

    return collection[0];
}

function size() {

    return collection.length;
}

function isEmpty() {

    if (collection.length ===0 ) {
        return true;
    }else {
        return false;
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};